import React from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Main from './Pages/Main';
import Users from './components/Users';
import User from './components/User';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Main} />
      <Route path="/users" component={Users}/>
      <Route path="/user/:id" render={rProps => <User {...rProps} />} />      
    </Switch>
  </BrowserRouter>
);   

export default App