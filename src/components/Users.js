import React, {useEffect, useState} from 'react';
import { Link, Route, useRouteMatch } from "react-router-dom";
import User from "./User";

const Users = () => {

  const [user, setUser] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
      const res = await fetch("https://jsonplaceholder.typicode.com/comments");
      const data = await res.json();
      setUser(data);
  };
  
  console.log(user);
    

  const { url } = useRouteMatch();
  const linkList = user.map((user) => {
    return (
      <li key={user.id}>
        <Link to={`${url}/${user.id}`}> {user.id} | {user.email} </Link>
      </li>
    );
  });


  return (
    <div>
      <h1>Users</h1><hr/>
        <Route path={`${url}/:userId`}>
          <User data={user} />
        </Route>
      
      <hr/>
      
        <ul>
          {linkList}
        </ul>

      </div>
    );
  };

export default Users;