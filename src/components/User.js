import React from "react";
import { useParams } from "react-router-dom";

const User = ({ data }) => {
  
  const { userId } = useParams();
  const user = data.find((p) => p.id === Number(userId));
  let userData;

  if (user) {
    userData = (
      <>
        <h1>{user.name}</h1>
        <p>{user.email}</p>
        <p>{user.body}</p>
      </>
    );
  } 
  
  else {
    userData = <h2>Chargement...</h2>;
  }

  return (
    <div>
        {userData}
    </div>
  );
};

export default User;